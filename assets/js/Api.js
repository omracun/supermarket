
const baseUrl = () => {
  return `${window.location.origin}`
}

const uploadImageUrl = `${window.location.origin}/assets/upload.php`
// const Authorization = 'Basic QktOZXdBcGlDSTRLb2Rla3JlYXRpdjpLb2RlS3JlYXRpdlN1a3Nlc1RlcnVz'

const getHeader = async () => {
  return {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
}

const generateParams = (params) => {
  let queryString = '';
  for (let key in params) {
    if (!params.hasOwnProperty(key)) continue;
    if (typeof params[key] === 'object') {
      params[key].forEach((item, index) => {
        for (let keyItem in item) {
          queryString += `${key}[${index}][${keyItem}]=${encodeURI(
            item[keyItem],
          )}&`;
        }
      });
    } else {
      queryString += `${key}=${encodeURIComponent(params[key])}&`;
    }
  }
  return queryString === '' ? '' : `?${queryString.replace(/&+$/, '')}`;
}

const logOut = async () => {
  // localStorage.setItem("user_session", null)
  // window.location.reload()
}

const ApiHelpers = {
  get: async (url, params) => {
    let Header = await getHeader();
    return new Promise((resolve) => {
      let uri = baseUrl() + url + generateParams(params);
      fetch(uri, {
        method: 'GET',
        credentials: "include",
        headers: {
          ...Header,
        },
      })
        .then((response) => { return response.json() })
        .then((responseData) => {
          if (responseData.status === 401) {
            logOut();
          } else {
            if (responseData.status === 404) {
              return resolve({
                status: 404,
                message: 'Page Not Found.',
              });
            } else {
              return resolve(responseData);
            }
          }
        })
        .catch((error) => {
          return resolve({
            status: 500,
            message: 'Terjadi Kesalahan Server.',
          });
        });
    });
  },
  post: async (url, body) => {
    let Header = await getHeader();
    return new Promise((resolve) => {
      fetch(baseUrl() + url, {
        method: 'POST',
        credentials: "include",
        headers: {
          ...Header,
        },
        body: JSON.stringify(body),
      })
        .then((response) => { return response.json() })
        .then((responseData) => {
          if (responseData.status === 401) {
            logOut();
          } else {
            if (responseData.status === 404) {
              return resolve({
                status: 404,
                message: 'Page Not Found.',
              });
            } else {
              return resolve(responseData);
            }
          }
        })
        .catch((error) => {
          return resolve({
            status: 500,
            message: 'Terjadi Kesalahan Server.',
            error
          });
        });
    });
  },
  uploadImage: async (body) => {
    return new Promise((resolve) => {
      fetch(uploadImageUrl, {
        method: 'POST',
        body: body,
      })
        .then((response) => { return response.json() })
        .then((responseData) => {
          return resolve(responseData);
        })
        .catch((error) => {
          return resolve({
            status: 500,
            message: 'Terjadi Kesalahan Server.',
          });
        });
    });
  },
}
