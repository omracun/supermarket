<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization, Token, User-Agent, Content-Length, Host");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
  die();
}
// error_reporting(-1);
ini_set('display_errors', 1);
ini_set('memory_limit', '-1');
set_time_limit(0);
header('Content-Type: application/json');

$response = array(
  'status' => 400,
  'message' => 'Gagal Upload Gambar!',
  'data' => (object)array()
);
$allowed_ext = array('png', 'jpg', 'jpeg');
$max_width = 720;
$max_height = 720;
$dir_year = date('Y') . DIRECTORY_SEPARATOR;
$dir_month = $dir_year . date('m') . DIRECTORY_SEPARATOR;
$dir_image = $dir_month . 'image' . DIRECTORY_SEPARATOR;
$base_url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/';
// $base_url = 'http://localhost:8080/';

if (!is_dir($dir_year)) {
  $make_dir =  mkdir($dir_year, 0777, true);
  if ($make_dir === true) {
    copy('index.php', $dir_year . 'index.php');
  }
}

if (!is_dir($dir_month)) {
  $make_dir =  mkdir($dir_month, 0777, true);
  if ($make_dir === true) {
    copy('index.php', $dir_month . 'index.php');
  }
}

if (!is_dir($dir_image)) {
  $make_dir =  mkdir($dir_image, 0777, true);
  if ($make_dir === true) {
    copy('index.php', $dir_image . 'index.php');
  }
}

$full_dir = __DIR__ . DIRECTORY_SEPARATOR . $dir_image;

if (isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
  $name = $_FILES['image']['name'];
  $x = explode('.', $name);
  $ext = strtolower(end($x));

  $file_tmp = $_FILES['image']['tmp_name'];

  $file_name = slug($x[0]) . '-' . time() . '.' . $ext;

  if (in_array($ext, $allowed_ext) === true) {
    $is_upload = move_uploaded_file($file_tmp, $full_dir . $name);
    if ($is_upload) {
      resize_crop_image($max_width, $max_height, $full_dir . $name, $full_dir . $file_name);
      if (file_exists($full_dir . $file_name)) {
        @unlink($full_dir . $name);
        $file_type = mime_content_type($full_dir);
        $type = explode('/', $file_type)[0];
        $response = array(
          'status' => 200,
          'message' => 'Berhasil Upload Gambar!',
          'data' => (object)array(
            'filetype' => $type,
            'filename' => $file_name,
            'filepath' => DIRECTORY_SEPARATOR . $dir_image,
            'fileuri' => $base_url . DIRECTORY_SEPARATOR . $dir_image . $file_name,
          )
        );
      } else {
        $response = array(
          'status' => 400,
          'message' => 'Gagal Upload Gambar! Gagal Proses Rezise',
          'data' => (object)array()
        );
      }
    } else {
      $response = array(
        'status' => 400,
        'message' => 'Gagal Upload Gambar! Gagal Proses Upload',
        'data' => (object)array()
      );
    }
  } else {
    $response = array(
      'status' => 400,
      'message' => 'Gagal Upload Gambar! Tipe file tidak diijinkan.',
      'data' => (object)array()
    );
  }
} else {
  if (isset($_POST['image']) && isset($_POST['name'])) {
    $data = $_POST['image'];

    list($type, $data) = explode(';', $data);
    list(, $data)      = explode(',', $data);
    $data = base64_decode($data);

    $name = $_POST['name'];
    $x = explode('.', $name);
    $ext = strtolower(end($x));

    $file_name = slug($x[0]) . '-' . time() . '.' . $ext;

    file_put_contents($full_dir . $name, $data);
    resize_crop_image($max_width, $max_height, $full_dir . $name, $full_dir . $file_name);
    if (file_exists($full_dir . $file_name)) {
      @unlink($full_dir . $name);
      $file_type = mime_content_type($full_dir);
      $type = explode('/', $file_type)[0];
      $response = array(
        'status' => 200,
        'message' => 'Berhasil Upload Gambar!',
        'data' => (object)array(
          'filetype' => $type,
          'filename' => $file_name,
          'filepath' => DIRECTORY_SEPARATOR . $dir_image,
          'fileuri' => $base_url . DIRECTORY_SEPARATOR . $dir_image . $file_name,
        )
      );
    } else {
      $response = array(
        'status' => 400,
        'message' => 'Gagal Upload Gambar! Gagal Proses Rezise',
        'data' => (object)array()
      );
    }
  } else {
    $response = array(
      'status' => 400,
      'message' => 'Gagal Upload Gambar! File tidak ditemukan',
      'data' => (object)array()
    );
  }
}

echo json_encode($response);


function slug($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

  // trim
  $text = trim($text, '-');

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // lowercase
  $text = strtolower($text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  if (empty($text)) {
    return 'n-a';
  }

  return substr($text, 0, 100);
}


function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 80)
{
  $imgsize = getimagesize($source_file);
  $width_new = $width = $imgsize[0];
  $height_new = $height = $imgsize[1];
  $mime = $imgsize['mime'];

  if ($width > $max_width && $height > $max_height) {
    $width_new = $max_width;
    $height_new = ceil(($width_new / $width) * $height);
  } else {
    if ($width > $max_width) {
      $width_new = $max_width;
      $height_new = ceil(($width_new / $width) * $height);
    } else if ($height > $max_height) {
      $height_new = $max_height;
      $height_new = ceil(($height_new / $height) * $width);
    }
  }

  switch ($mime) {
    case 'image/gif':
      $image_create = "imagecreatefromgif";
      $image = "imagegif";
      break;

    case 'image/png':
      $image_create = "imagecreatefrompng";
      $image = "imagejpeg";
      $quality = 75;
      break;

    case 'image/jpeg':
      $image_create = "imagecreatefromjpeg";
      $image = "imagejpeg";
      $quality = 80;
      break;

    default:
      return false;
      break;
  }

  $dst_img = imagecreatetruecolor($width_new, $height_new);
  $src_img = $image_create($source_file);
  if ($mime == 'image/png') {
    $white = imagecolorallocate($dst_img,  255, 255, 255);
    imagefilledrectangle($dst_img, 0, 0, $width_new, $height_new, $white);
  }

  imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $width_new, $height_new, $width, $height);

  $image($dst_img, $dst_dir, $quality);

  if ($dst_img) {
    imagedestroy($dst_img);
  }

  if ($src_img) {
    imagedestroy($src_img);
  }
}
