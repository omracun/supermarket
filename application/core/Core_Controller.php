<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Core_Controller extends CI_Controller
{

  public function __construct()
  {
    if (empty($_POST) && $_SERVER['REQUEST_METHOD'] === 'POST') {
      $_POST = json_decode(file_get_contents('php://input'), true);
      if (!empty($_POST)) {
        foreach ($_POST as $key => $value) {
          if (endsWith($key, 'jsonobject')) {
            if (!empty($value)) {
              $_POST[$key] = json_encode($value);
            } else {
              $_POST[$key] = '{}';
            }
          }
          if (endsWith($key, 'bool')) {
            $_POST[$key] = $value === true ? '1' : '0';
          }
          if (endsWith($key, 'jsonarray') || $key == 'item') {
            if (!empty($value)) {
              $_POST[$key] = json_encode($value);
            } else {
              $_POST[$key] = '[]';
            }
          }
        }
      }
    } else {
      foreach ($_POST as $key => $value) {
        if (endsWith($key, 'bool')) {
          $_POST[$key] = $value === 'true' ? '1' : '0';
        }
      }
    }
    if (!empty($_GET)) {
      foreach ($_GET as $key => $value) {
        if (endsWith($key, 'bool')) {
          $_GET[$key] = $value === 'true' ? '1' : '0';
        }
      }
    }
    parent::__construct();
    $this->form_validation->set_error_delimiters('', '');
  }

  public function responseMultiData($status = 200, $message = 'Success', $data = array(), $error_code = '')
  {
    header("Content-Type: application/json");
    header("HTTP/1.1 200 OK");
    echo json_encode(array(
      'status'    => $status,
      'message'   => $message,
      'error_code' => $error_code,
      'results'      => (object)$data
    ));
    exit;
  }

  public function responseSingleData($status = 200, $message = 'Success', $data = array(), $error_code = '')
  {
    header("Content-Type: application/json");
    header("HTTP/1.1 200 OK");
    echo json_encode(array(
      'status'    => $status,
      'message'   => $message,
      'error_code' => $error_code,
      'results'      => (object)array(
        'data' => (object) $data
      )
    ));
    exit;
  }

  public function responseWithoutData($status = 200, $message = 'Success', $error_code = '')
  {
    header("Content-Type: application/json");
    header("HTTP/1.1 200 OK");
    echo json_encode(array(
      'status'    => $status,
      'message'   => $message,
      'error_code' => $error_code,
      'results'      => (object)array(
        'data' => (object) array()
      )
    ));
    exit;
  }

  public function responseValidationError($data)
  {
    header("Content-Type: application/json");
    header("HTTP/1.1 200 OK");
    echo json_encode(array(
      'status'    => 412,
      'message'   => 'error_validation',
      'error_code' => '',
      'results'   => (object)array('data' => $data->error_array(), 'message' => $data->error_string())
    ));
    exit;
  }

  public function unauthorized()
  {
    header("Content-Type: application/json");
    header("HTTP/1.1 401 Unauthorized");
    header("WWW-Authenticate: Bearer realm=\"app\"");
    echo json_encode(array(
      "status"    => 401,
      "message"   => 'Unauthorized',
      'error_code' => '',
      'results'   => (object)array(
        'data' => (object) array()
      )
    ));
    exit;
  }
}

/* End of file Core_Api_Controller.php */
