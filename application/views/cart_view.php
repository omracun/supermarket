<div class="d-flex flex-column" id="app">
  <div class="mt-3">
    <h3>Keranjang Belanja</h3>
    <div class="mt-2">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Barang</th>
            <th class="text-end" scope="col">Harga</th>
            <th class="text-end" scope="col">Jumlah</th>
            <th scope="col">SubTotal</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(item, index) in dataProduct">
            <th scope="row">{{index+1}}</th>
            <td>{{item.name}}</td>
            <td class="text-end">{{item.price | idFormat}}</td>
            <td class="text-end">{{item.qty | idFormat}}</td>
            <td class="text-end">{{item.subtotal | idFormat}}</td>
            <td>
              <span class="btn btn-primary" title="Tambah" @click="addAction(item)">Tambah</span>
              <span class="btn btn-warning" title="Kurang" @click="minusAction(item)">Kurang</span>
              <span class="btn btn-danger" title="Hapus" @click="removeAction(item)">Hapus</span>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<script>
  let vue = new Vue({
    el: '#app',
    name: 'ganti Password',
    data: {
      toInsertData: false,
      dataProduct: [],
      pagination: {},
    },
    mounted: function() {
      this.getData()
    },
    methods: {
      getData: async function() {
        this.isFetchData = true
        let data = await ApiHelpers.get('/home/get_cart', {})
        this.dataProduct = data.results.data
        this.isFetchData = false
      },
      addAction: async function(item) {
        let dataInsert = await ApiHelpers.post('/home/add_cart', {
          id: item.product_id,
        })
        if (dataInsert.status === 200) {
          this.getData()
        } else {
          alert('Gagal menambah jumlah')
        }
        this.getData()
      },
      minusAction: async function(item) {
        let dataInsert = await ApiHelpers.post('/home/minus_cart', {
          id: item.product_id,
        })
        if (dataInsert.status === 200) {
          this.getData()
        } else {
          alert('Gagal mengurangi jumlah')
        }
        this.getData()
      },
      removeAction: async function(item) {
        let dataInsert = await ApiHelpers.post('/home/remove_cart', {
          id: item.id,
        })
        if (dataInsert.status === 200) {
          this.getData()
        } else {
          alert('Gagal menghapus keranjang belanja')
        }
        this.getData()
      }
    }
  })
</script>