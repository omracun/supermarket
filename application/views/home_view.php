<div class="d-flex flex-column" id="app">
  <h3>Input Produk</h3>
  <form>
    <div class="mb-3">
      <label class="form-label">Nama </label>
      <input type="text" v-model="form.name" class="form-control" placeholder="Nama">
    </div>
    <div class="mb-3">
      <label class="form-label">Harga </label>
      <input type="number" v-model="form.price" class="form-control" placeholder="Harga">
    </div>
    <div class="mb-3">
      <label class="form-label">Stok </label>
      <input type="number" v-model="form.stok" class="form-control" placeholder="Stok">
    </div>
    <div class="mb-3">
      <label for="formFile" class="form-label">Gambar</label>
      <input class="form-control" type="file" id="formFile" accept=".jpg, .png">
    </div>
    <div class="mb-3">
      <div class="alert" :class="isError.class" v-if="isError.status" role=" alert">
        {{ isError.message }}
      </div>
    </div>
    <div class="mb-3">
      <button type="button" class="btn btn-success waves-effect waves-light m-r-10" @click="insertProduct">
        <span v-if="!toInsertData">Simpan</span>
        <span v-if="toInsertData">Process..</span>
      </button>
    </div>
  </form>
  <div class="mt-3">
    <h3>Data Produk</h3>
    <div class="mt-2">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama Barang</th>
            <th class="text-end" scope="col">Harga</th>
            <th class="text-end" scope="col">Stok</th>
            <th scope="col">Gambar</th>
            <th class="text-center" scope="col">Kode</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(item, index) in dataProduct">
            <th scope="row">{{index+1}}</th>
            <td>{{item.name}}</td>
            <td class="text-end">{{item.price | idFormat}}</td>
            <td class="text-end">{{item.stok | idFormat}}</td>
            <td>{{item.image_url}}</td>
            <td class="text-center">{{item.code}}</td>
            <td>
              <span class="btn btn-primary" title="Beli" @click="buyAction(item)">Beli</span>
            </td>
          </tr>
        </tbody>
      </table>
      <div class="d-flex flex-row">
        <ul class="pagination">
          <li v-if="pagination.first_page"><a class="page-link" @click="getData( 1)">First Page</a></li>
        </ul>
        <ul class="pagination" v-for="pagi in pagination.detail">
          <li class="page-item" v-if="pagi != pagination.current"><a class="page-link" @click="getData(pagi)">{{ pagi }}</a></li>
          <li class="page-item active" v-else><a class="page-link">{{ pagi }}</a></li>
        </ul>
        <ul class="pagination">
          <li v-if="pagination.last_page"><a class="page-link" @click="getData(pagination.total_page)">Last Page</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

<script>
  let vue = new Vue({
    el: '#app',
    name: 'ganti Password',
    data: {
      form: {
        name: '',
        price: 0,
        stok: 0,
        image_url: ''
      },
      toInsertData: false,
      isError: {
        status: false,
        message: '',
        class: 'alert-primary'
      },
      dataProduct: [],
      pagination: {},
      isFetchData: false,
    },
    mounted: function() {
      this.getData()
    },
    methods: {
      getData: async function(page = 1) {
        this.isFetchData = true
        let data = await ApiHelpers.get('/home/get_data', {
          page,
          limit: 10
        })
        this.dataProduct = data.results.data
        this.pagination = data.results.pagination
        this.isFetchData = false
      },
      validator: function() {
        let form = this.form;
        let check = {
          valid: true,
          message: ''
        }

        if (form.name.length < 1) {
          return check = {
            valid: false,
            message: 'Nama tidak boleh kosong!'
          }
        } else if ((form.price * 1) <= 0) {
          return check = {
            valid: false,
            message: 'Harga tidak boleh kosong!'
          }
        } else if ((form.stok * 1) <= 0) {
          return check = {
            valid: false,
            message: 'Stok tidak boleh kosong!'
          }
        } else if (form.image_url.length < 0) {
          return check = {
            valid: false,
            message: 'Gambar tidak boleh kosong!'
          }
        } else {
          return check = {
            valid: true,
            message: ''
          }
        }
      },
      insertProduct: async function() {
        let self = this
        let check = self.validator()
        // if (check.valid === true) {
        if (!self.toInsertData) {
          console.log('masuk')
          self.toInsertData = true
          let dataInsert = await ApiHelpers.post('/home/insert', self.form)
          if (dataInsert.status === 200) {
            self.isError = {
              status: true,
              message: 'Berhasil Input Data Produk',
              class: 'alert-success'
            }
            self.toInsertData = false
            self.getData()
            self.form = {
              name: '',
              price: 0,
              stok: 0,
              image_url: ''
            }
          } else {
            self.isError = {
              status: true,
              message: dataInsert.status === 412 ? dataInsert.results.message : dataInsert.message,
              class: 'alert-error'
            }
            self.toInsertData = false
          }
        }
        // } else {
        //   self.isError = {
        //     status: true,
        //     message: check.message,
        //     class: 'alert-warning'
        //   }
        // }
      },
      buyAction: async function(item) {
        let dataInsert = await ApiHelpers.post('/home/buy', {
          id: item.id,
          price: item.price
        })
        if (dataInsert.status === 200) {
          alert('Berhasil Memasukan ke keranjang belanja')
        } else {
          alert('Gagal Memasukan ke keranjang belanja')
        }
        this.getData()
      }
    }
  })
</script>