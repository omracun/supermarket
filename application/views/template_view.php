<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="googlebot" content="noindex" />
  <meta name="googlebot" content="nofollow" />
  <meta name="googlebot-news" content="noindex" />
  <meta name="googlebot-news" content="nosnippet" />
  <meta name="googlebot-news" content="nofollow" />
  <meta name="robots" content="noindex" />
  <meta name="robots" content="nofollow" />
  <title><?php template_echo('title'); ?> | Supermarket</title>

  <?php if (ENVIRONMENT === 'development') { ?>
    <!-- VueJS -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
  <?php } else { ?>
    <!-- VueJS -->
    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
  <?php } ?>

  <script src="https://cdn.jsdelivr.net/npm/moment@2.29.4/moment.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/moment@2.29.4/locale/id.js"></script>

  <script src="/assets/js/Api.js"></script>


  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>

  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>

  <script>
    Vue.filter('idFormat', function (nStr) {
        nStr += '';
        let x = nStr.split('.');
        let x1 = x[0];
        let x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return  x1 + x2;
    });
    // Vue.filter('percent', function (nStr) {
    //     return  nStr.replace('.',',');
    // });
    // Vue.filter('dateID', function (nStr) {
    //   return moment(nStr).format('dddd, DD MMM YYYY') != 'Invalid date' ? moment(nStr).format('dddd, DD MMM YYYY') : '-'
    // });
    // Vue.filter('dateTimeID', function (nStr) {
    //   return moment(nStr).format('DD MMM YYYY HH:mm:ss') != 'Invalid date' ? moment(nStr).format('DD MMM YYYY HH:mm:ss') : '-'
    // });
  </script>

</head>

<body>
  <div class="container py-5">
    <?php template_echo('content'); ?>

  </div>
</body>

</html>