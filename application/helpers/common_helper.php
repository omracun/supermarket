<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

if (!function_exists('pr')) {

  function pr($arr)
  {
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
  }
}

if (!function_exists('is_nominal')) {

  function is_nominal($value)
  {
    if (is_numeric($value)) {
      if (strpos($value, ".") !== false) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}

if (!function_exists('sapaan_waktu')) {

  function sapaan_waktu($time = null)
  {
    if(empty($time)){
      $time = date('H:i');
    }
    if ($time >= '00:00' && $time <= '02:59') {
      return 'Malam';
    }
    if ($time >= '03:00' && $time <= '09:59') {
      return 'Pagi';
    }
    if ($time >= '10:00' && $time <= '14:59') {
      return 'Siang';
    }
    if ($time >= '15:00' && $time <= '18:59') {
      return 'Sore';
    }
    if ($time >= '19:00' && $time <= '23:59') {
      return 'Malam';
    }

  }
}

if (!function_exists('romanic_number')) {
  function romanic_number($integer, $upcase = true)
  {
    $table = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
    $return = '';
    while ($integer > 0) {
      foreach ($table as $rom => $arb) {
        if ($integer >= $arb) {
          $integer -= $arb;
          $return .= $rom;
          break;
        }
      }
    }
    return $return;
  }
}

if (!function_exists('convertNullToString')) {
  function convertNullToString($v)
  {
    return (is_null($v)) ? "" : $v;
  }
}

if (!function_exists('getFirstParagrap')) {
  function getFirstParagrap($string)
  {
    $string = substr($string, 0, strpos($string, "</p>") + 4);
    return $string;
  }
}

if (!function_exists('startsWith')) {

  function startsWith($haystack, $needle)
  {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
  }
}

if (!function_exists('bulat_rp')) {

  function bulat_rp($uang)
  {
    $akhir = 0;
    $sisa_angka = substr($uang, -2);
    if ($sisa_angka != '00') {
      if ($sisa_angka < 100) {
        $akhir = $uang + (100 - $sisa_angka);
        return $akhir;
      } else {
        return $uang;
      }
    } else {
      return $uang;
    }
  }
}

if (!function_exists('bulat_rp_bawah')) {
  if (!function_exists('bulat_rp_bawah')) {

    function bulat_rp_bawah($uang)
    {
      $akhir = 0;
      $sisa_angka = substr($uang, -2);
      if ($sisa_angka != '00') {
        $akhir = $uang - $sisa_angka;
        return $akhir;
      } else {
        return $uang;
      }
    }
  }
}

if (!function_exists('convert_month')) {

  function convert_month($month, $lang = 'en')
  {
    $month = (int) $month;
    switch ($lang) {
      case 'id':
        $arr_month = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember');
        break;

      default:
        $arr_month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        break;
    }
    $month_converted = $arr_month[$month - 1];

    return $month_converted;
  }
}

if (!function_exists('convert_date')) {

  function convert_date($date, $type = 'num', $format = '.', $lang = 'en')
  {
    if (!empty($date)) {
      $date = substr($date, 0, 10);
      if ($type == 'num') {
        $date_converted = str_replace('-', $format, $date);
      } else {
        $year = substr($date, 0, 4);
        $month = substr($date, 5, 2);
        $month = convert_month($month, $lang);
        $day = substr($date, 8, 2);

        $date_converted = $day . ' ' . $month . ' ' . $year;
      }
    } else {
      $date_converted = '-';
    }
    return $date_converted;
  }
}

if (!function_exists('convert_datetime')) {

  function convert_datetime($date, $type = 'num', $formatdate = '.', $formattime = ':', $lang = 'en')
  {

    if (!empty($date)) {
      if ($type == 'num') {
        $date_converted = str_replace('-', $formatdate, str_replace(':', $formattime, $date));
      } else {
        $year = substr($date, 0, 4);
        $month = substr($date, 5, 2);
        $month = convert_month($month, $lang);
        $day = substr($date, 8, 2);
        $time = strlen($date) > 10 ? substr($date, 11, 8) : '';
        $time = str_replace(':', $formattime, $time);

        $date_converted = $day . ' ' . $month . ' ' . $year . ' Pukul ' . $time . ' WIB';
      }
    } else {
      $date_converted = '-';
    }
    return $date_converted;
  }
}

if (!function_exists('get_filesize')) {

  function get_filesize($file)
  {
    $bytes = array("B", "KB", "MB", "GB", "TB", "PB");
    $file_with_path = $file;
    $file_with_path;
    // replace (possible) double slashes with a single one
    $file_with_path = str_replace("///", "/", $file_with_path);
    $file_with_path = str_replace("//", "/", $file_with_path);
    $size = @filesize($file_with_path);
    $i = 0;

    //divide the filesize (in bytes) with 1024 to get "bigger" bytes
    while ($size >= 1024) {
      $size = $size / 1024;
      $i++;
    }

    // you can change this number if you like (for more precision)
    if ($i > 1) {
      return round($size, 1) . " " . $bytes[$i];
    } else {
      return round($size, 0) . " " . $bytes[$i];
    }
  }
}

if (!function_exists('terbilang')) {

  function terbilang($x)
  {
    $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    if ($x < 12)
      return " " . $abil[$x];
    elseif ($x < 20)
      return terbilang($x - 10) . "belas";
    elseif ($x < 100)
      return terbilang($x / 10) . " puluh" . terbilang($x % 10);
    elseif ($x < 200)
      return " seratus" . Terbilang($x - 100);
    elseif ($x < 1000)
      return terbilang($x / 100) . " ratus" . terbilang($x % 100);
    elseif ($x < 2000)
      return " seribu" . terbilang($x - 1000);
    elseif ($x < 1000000)
      return terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
    elseif ($x < 1000000000)
      return terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
  }
}

if (!function_exists('make_thumb_admin')) {

  function make_thumb_admin($src, $dest, $desired_width, $desired_height)
  {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);

    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
  }
}

if (!function_exists('makeThumbnails')) {

  function makeThumbnails($updir, $img)
  {
    $thumbnail_width = 80;
    $thumbnail_height = 80;
    $thumb_beforeword = "thumb";
    $arr_image_details = getimagesize("$updir" . "$img"); // pass id to thumb name
    $original_width = $arr_image_details[0];
    $original_height = $arr_image_details[1];
    if ($original_width > $original_height) {
      $new_width = $thumbnail_width;
      $new_height = intval($original_height * $new_width / $original_width);
    } else {
      $new_height = $thumbnail_height;
      $new_width = intval($original_width * $new_height / $original_height);
    }
    $dest_x = intval(($thumbnail_width - $new_width) / 2);
    $dest_y = intval(($thumbnail_height - $new_height) / 2);
    if ($arr_image_details[2] == 1) {
      $imgt = "ImageGIF";
      $imgcreatefrom = "ImageCreateFromGIF";
    }
    if ($arr_image_details[2] == 2) {
      $imgt = "ImageJPEG";
      $imgcreatefrom = "ImageCreateFromJPEG";
    }
    if ($arr_image_details[2] == 3) {
      $imgt = "ImagePNG";
      $imgcreatefrom = "ImageCreateFromPNG";
    }
    if ($imgt) {
      $old_image = $imgcreatefrom("$updir" . "$img");
      $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
      imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
      $imgt($new_image, "$updir" . "$thumb_beforeword" . "$img");
    }
  }
}

if (!function_exists('resize_crop_image')) {

  function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 80)
  {
    $imgsize = getimagesize($source_file);
    $width = $imgsize[0];
    $height = $imgsize[1];
    $mime = $imgsize['mime'];

    switch ($mime) {
      case 'image/gif':
        $image_create = "imagecreatefromgif";
        $image = "imagegif";
        break;

      case 'image/png':
        $image_create = "imagecreatefrompng";
        $image = "imagepng";
        $quality = 7;
        break;

      case 'image/jpeg':
        $image_create = "imagecreatefromjpeg";
        $image = "imagejpeg";
        $quality = 80;
        break;

      default:
        return false;
        break;
    }

    $dst_img = imagecreatetruecolor($max_width, $max_height);
    if ($mime == 'image/png') {
      imagealphablending($dst_img, false);
      imagesavealpha($dst_img, true);
      $transparent = imagecolorallocatealpha($dst_img, 255, 255, 255, 127);
      imagefilledrectangle($dst_img, 0, 0, $max_width, $max_height, $transparent);
    }
    $src_img = $image_create($source_file);

    $width_new = $height * $max_width / $max_height;
    $height_new = $width * $max_height / $max_width;
    //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
    if ($width_new > $width) {
      //cut point by height
      $h_point = (($height - $height_new) / 2);
      //copy image
      imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
    } else {
      //cut point by width
      $w_point = (($width - $width_new) / 2);
      imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
    }

    $image($dst_img, $dst_dir, $quality);

    if ($dst_img) {
      imagedestroy($dst_img);
    }

    if ($src_img) {
      imagedestroy($src_img);
    }
  }
}

if (!function_exists('getimagesizefromstring')) {
  function getimagesizefromstring($string_data)
  {
    $uri = 'data://application/octet-stream;base64,'  . base64_encode($string_data);
    return getimagesize($uri);
  }
}

if (!function_exists('resize_crop_image_blob_gif')) {

  function resize_crop_image_blob_gif($max_width, $max_height, $source_file, $dst_dir, $quality = 80)
  {
    $imgsize = getimagesize($source_file);
    $width = $imgsize[0];
    $height = $imgsize[1];

    $dst_img = imagecreatetruecolor($max_width, $max_height);
    $image = "imagegif";
    $src_img = imagecreatefromgif($source_file);


    $width_new = $height * $max_width / $max_height;
    $height_new = $width * $max_height / $max_width;
    //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
    if ($width_new > $width) {
      //cut point by height
      $h_point = (($height - $height_new) / 2);
      //copy image
      imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
    } else {
      //cut point by width
      $w_point = (($width - $width_new) / 2);
      imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
    }

    $image($dst_img, $dst_dir, $quality);

    if ($dst_img) {
      imagedestroy($dst_img);
    }

    if ($src_img) {
      imagedestroy($src_img);
    }
  }
}

if (!function_exists('resize_crop_image_blob_png')) {

  function resize_crop_image_blob_png($max_width, $max_height, $source_file, $dst_dir, $quality = 80)
  {
    $imgsize = getimagesize($source_file);
    $width = $imgsize[0];
    $height = $imgsize[1];

    $dst_img = imagecreatetruecolor($max_width, $max_height);
    $image = "imagepng";
    $quality = 7;
    $src_img = imagecreatefrompng($source_file);


    $width_new = $height * $max_width / $max_height;
    $height_new = $width * $max_height / $max_width;
    //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
    if ($width_new > $width) {
      //cut point by height
      $h_point = (($height - $height_new) / 2);
      //copy image
      imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
    } else {
      //cut point by width
      $w_point = (($width - $width_new) / 2);
      imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
    }

    $image($dst_img, $dst_dir, $quality);

    if ($dst_img) {
      imagedestroy($dst_img);
    }

    if ($src_img) {
      imagedestroy($src_img);
    }
  }
}

if (!function_exists('resize_crop_image_blob_jpeg')) {

  function resize_crop_image_blob_jpeg($max_width, $max_height, $source_file, $dst_dir, $quality = 80)
  {
    $imgsize = getimagesize($source_file);
    $width = $imgsize[0];
    $height = $imgsize[1];

    $dst_img = imagecreatetruecolor($max_width, $max_height);
    $src_img = imagecreatefromjpeg($source_file);
    $image = "imagejpeg";

    $width_new = $height * $max_width / $max_height;
    $height_new = $width * $max_height / $max_width;
    //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
    if ($width_new > $width) {
      //cut point by height
      $h_point = (($height - $height_new) / 2);
      //copy image
      imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
    } else {
      //cut point by width
      $w_point = (($width - $width_new) / 2);
      imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
    }

    $image($dst_img, $dst_dir, $quality);

    if ($dst_img) {
      imagedestroy($dst_img);
    }

    if ($src_img) {
      imagedestroy($src_img);
    }
  }
}

if (!function_exists('validate_date')) {

  function validate_date($date, $format = 'Y-m-d')
  {
    if ($format == 'Y-m') {
      $date = $date . '-01';
      $format = 'Y-m-d';
    }
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
  }
}

if (!function_exists('pageGenerate')) {

  function page_generate($total, $pagenum, $limit)
  {
    $total_page = ceil($total / $limit);

    //------------- Prev page
    $prev = $pagenum - 1;
    if ($prev < 1) {
      $prev = 0;
    }
    //------------------------

    //------------- Next page
    $next = $pagenum + 1;
    if ($next > $total_page) {
      $next = 0;
    }
    //----------------------

    $from = 1;
    $to = $total_page;

    $to_page = $pagenum - 2;
    if ($to_page > 0) {
      $from = $to_page;
    }

    if ($total_page >= 5) {
      if ($total_page > 0) {
        $to = 5 + $to_page;
        if ($to > $total_page) {
          $to = $total_page;
        }
      } else {
        $to = 5;
      }
    }

    #looping kotak pagination
    $firstpage_istrue = false;
    $lastpage_istrue = false;
    $detail = [];
    if ($total_page <= 1) {
      $detail = [];
    } else {
      for ($i = $from; $i <= $to; $i++) {
        $detail[] = $i;
      }
      if ($from != 1) {
        $firstpage_istrue = true;
      }
      if ($to != $total_page) {
        $lastpage_istrue = true;
      }
    }

    $total_display = 0;
    if ($pagenum < $total_page) {
      $total_display = $limit;
    }
    if ($pagenum == $total_page) {
      if (($total % $limit) != 0) {
        $total_display = $total % $limit;
      } else {
        $total_display = $limit;
      }
    }

    $pagination = array(
      'total_data' => $total,
      'total_page' => $total_page,
      'total_display' => $total_display,
      'first_page' => $firstpage_istrue,
      'last_page' => $lastpage_istrue,
      'prev' => $prev,
      'current' => $pagenum,
      'next' => $next,
      'detail' => $detail
      // 'detail' => json_encode($detail)
    );

    return $pagination;
  }
}

if (!function_exists('filterQuery')) {
  function filter_query($where_filter = array(), $field_allowed = array())
  {
    $sql_search = '';

    if ($where_filter != null) {
      foreach ($where_filter as $row) {
        $type = isset($row['type']) ? $row['type'] : '';
        $field = isset($row['field']) ? $row['field'] : '';
        $value = isset($row['value']) ? $row['value'] : '';
        $comparison = isset($row['comparison']) ? $row['comparison'] : '';

        if (!in_array($field, $field_allowed)) {
          $field = '';
        }

        if ($field == '' || $value == '') {
          $type = '';
        }

        switch ($type) {
          case 'string':
            $arr_allowed = array('=', '<', '>', '<>');
            if (!in_array($comparison, $arr_allowed)) {
              $comparison = '=';
            }
            switch ($comparison) {
              case '=':
                $sql_search .= " AND " . $field . " = '" . $value . "'";
                break;
              case '<':
                $sql_search .= " AND " . $field . " LIKE '" . $value . "%'";
                break;
              case '>':
                $sql_search .= " AND " . $field . " LIKE '%" . $value . "'";
                break;
              case '<>':
                $sql_search .= " AND " . $field . " LIKE '%" . $value . "%'";
                break;
            }
            break;
          case 'numeric':
            if (is_numeric($value)) {
              $arr_allowed = array('=', '<', '>', '<=', '>=', '<>');
              if (!in_array($comparison, $arr_allowed)) {
                $comparison = '=';
              }
              $sql_search .= " AND " . $field . " " . $comparison . " " . $value;
            }
            break;
          case 'list':
            if (strstr($value, '::')) {
              $arr_allowed = array('yes', 'no', 'bet');
              if (!in_array($comparison, $arr_allowed)) {
                $comparison = 'yes';
              }
              $fi = explode('::', $value);
              for ($q = 0; $q < count($fi); $q++) {
                $fi[$q] = "'" . $fi[$q] . "'";
              }
              $value = implode(',', $fi);
              if ($comparison == 'yes') {
                $sql_search .= " AND " . $field . " IN (" . $value . ")";
              }
              if ($comparison == 'no') {
                $sql_search .= " AND " . $field . " NOT IN (" . $value . ")";
              }
              if ($comparison == 'bet') {
                $sql_search .= " AND " . $field . " BETWEEN " . $fi[0] . " AND " . $fi[1];
              }
            } else {
              $sql_search .= " AND " . $field . " = '" . $value . "'";
            }
            break;
          case 'date':
            if (endsWith($field, 'date') || endsWith($field, 'datetime')) {
              $value1 = '';
              $value2 = '';
              if (strstr($value, '::')) {
                $date_value = explode('::', $value);
                $value1 = $date_value[0];
                $value2 = $date_value[1];
              } else {
                $value1 = $value;
              }

              if (endsWith($field, 'datetime') && validate_date($value1, 'Y-m-d')) {
                $field = 'date(' . $field . ')';
              }

              $arr_allowed = array('=', '<', '>', '<=', '>=', '<>', 'bet');
              if (!in_array($comparison, $arr_allowed)) {
                $comparison = '=';
              }
              if ($comparison == 'bet') {
                if (validate_date($value1) && validate_date($value2)) {
                  $sql_search .= " AND " . $field . " BETWEEN '" . $value1 . "' AND '" . $value2 . "'";
                }
              } else {
                if (validate_date($value1) || validate_date($value1, 'Y-m-d H:i:s')) {
                  $sql_search .= " AND " . $field . " " . $comparison . " '" . $value1 . "'";
                }
              }
            }
            break;
        }
      }
    }

    return $sql_search;
  }
}

if(!function_exists('searchQuery')){
  function search_query($search, $field){
    $query = '';
    if(is_array($field)){
      foreach($field as $row){
        if(!empty($search)){
          if(!endsWith($row, 'datetime') && !endsWith($row, 'date')){
            $query .=  $row . ' LIKE \'%' . $search . '%\' OR ';
          }
        }
      }
    }
    return $query == '' ? '' : ' AND (' . rtrim($query, 'OR ') . ') ' ;
  }
}

if (!function_exists('slug')) {
  function slug($text)
  {
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

    // trim
    $text = trim($text, '-');

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // lowercase
    $text = strtolower($text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    if (empty($text)) {
      return 'n-a';
    }

    return $text;
  }  
}

if (!function_exists('real_ip')) {
  function real_ip()
  {
    $ip = '';

    if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
      $ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
    } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
      $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
  }
}

if(!function_exists('safeEncrypt')){
  function safeEncrypt(string $message, string $key)
  {
    if (mb_strlen($key, '8bit') !== SODIUM_CRYPTO_SECRETBOX_KEYBYTES) {
      throw new RangeException('Key is not the correct size (must be 32 bytes).');
    }
    $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);

    $cipher = base64_encode(
      $nonce .
        sodium_crypto_secretbox(
          $message,
          $nonce,
          $key
        )
    );
    sodium_memzero($message);
    sodium_memzero($key);
    $cipher = urlencode(strtr($cipher, '+/=', '-_,'));
    return $cipher;
  }
}

if (!function_exists('nominal_convert')) {
  function nominal_convert($size)
  {
    $bytes = array("", "rb", "jt", "M", "T");

    $i = 0;

    //divide the filesize (in bytes) with 1024 to get "bigger" bytes
    while ($size >= 1000 && $i < 4) {
      $size = $size / 1000;
      $i++;
    }

    // you can change this number if you like (for more precision)
    if (is_decimal($size)) {
      if ($i > 0) {
        return number_format(round($size, 1), 1, ',', '.') . " " . $bytes[$i];
      } else {
        return number_format(round($size, 0), 1, ',', '.') . " " . $bytes[$i];
      }
    } else {
      if ($i > 0) {
        return number_format(round($size, 1), 0, ',', '.') . " " . $bytes[$i];
      } else {
        return number_format(round($size, 0), 0, ',', '.') . " " . $bytes[$i];
      }
    }
  }
}

if (!function_exists('is_decimal')) {

  function is_decimal($val)
  {
    return is_numeric($val) && floor($val) != $val;
  }
}

if (!function_exists('filterParamsArray')) {
  function filter_params_array($where_filter = array(), $field_allowed = array())
  {
    $sql_search = '';

    if ($where_filter != null) {
      foreach ($where_filter as $row) {
        $type = isset($row['type']) ? $row['type'] : '';
        $field = isset($row['field']) ? $row['field'] : '';
        $value = isset($row['value']) ? $row['value'] : '';
        $comparison = isset($row['comparison']) ? $row['comparison'] : '';

        if (!isset($field_allowed[$field])) {
          $field = '';
        } else {
          $field = $field_allowed[$field];
        }

        if ($field == '' || $value == '') {
          $type = '';
        }

        switch ($type) {
          case 'string':
            $arr_allowed = array('=', '<', '>', '<>', '!=');
            if (!in_array($comparison, $arr_allowed)) {
              $comparison = '=';
            }
            switch ($comparison) {
              case '=':
                $sql_search .= " AND " . $field . " = '" . $value . "'";
                break;
              case '!=':
                $sql_search .= " AND " . $field . " != '" . $value . "'";
                break;
              case '<':
                $sql_search .= " AND " . $field . " LIKE '" . $value . "%'";
                break;
              case '>':
                $sql_search .= " AND " . $field . " LIKE '%" . $value . "'";
                break;
              case '<>':
                $sql_search .= " AND " . $field . " LIKE '%" . $value . "%'";
                break;
            }
            break;
          case 'numeric':
            if (is_numeric($value)) {
              $arr_allowed = array('=', '<', '>', '<=', '>=', '<>');
              if (!in_array($comparison, $arr_allowed)) {
                $comparison = '=';
              }
              $sql_search .= " AND " . $field . " " . $comparison . " " . $value;
            }
            break;
          case 'list':
            if (strstr($value, '::')) {
              $arr_allowed = array('yes', 'no', 'bet');
              if (!in_array($comparison, $arr_allowed)) {
                $comparison = 'yes';
              }
              $fi = explode('::', $value);
              for ($q = 0; $q < count($fi); $q++) {
                $fi[$q] = "'" . $fi[$q] . "'";
              }
              $value = implode(',', $fi);
              if ($comparison == 'yes') {
                $sql_search .= " AND " . $field . " IN (" . $value . ")";
              }
              if ($comparison == 'no') {
                $sql_search .= " AND " . $field . " NOT IN (" . $value . ")";
              }
              if ($comparison == 'bet') {
                $sql_search .= " AND " . $field . " BETWEEN " . $fi[0] . " AND " . $fi[1];
              }
            } else {
              $sql_search .= " AND " . $field . " = '" . $value . "'";
            }
            break;
          case 'date':
            if (endsWith($field, 'date')) {
              $value1 = '';
              $value2 = '';
              if (strstr($value, '::')) {
                $date_value = explode('::', $value);
                $value1 = $date_value[0];
                $value2 = $date_value[1];
              } else {
                $value1 = $value;
              }

              $arr_allowed = array('=', '<', '>', '<=', '>=', '<>', 'bet');
              if (!in_array($comparison, $arr_allowed)) {
                $comparison = '=';
              }
              if ($comparison == 'bet') {
                if (validate_date($value1) && validate_date($value2)) {
                  $sql_search .= " AND " . $field . " BETWEEN '" . $value1 . "' AND '" . $value2 . "'";
                }
              } else {
                if (validate_date($value1)) {
                  $sql_search .= " AND " . $field . " " . $comparison . " '" . $value1 . "'";
                }
              }
            }
            if (endsWith($field, 'datetime')) {
              $value1 = '';
              $value2 = '';
              if (strstr($value, '::')) {
                $date_value = explode('::', $value);
                $value1 = $date_value[0];
                $value2 = $date_value[1];
              } else {
                $value1 = $value;
              }

              $arr_allowed = array('=', '<', '>', '<=', '>=', '<>', 'bet');
              if (!in_array($comparison, $arr_allowed)) {
                $comparison = '=';
              }
              if ($comparison == 'bet') {
                if (validate_date($value1, 'Y-m-d H:i:s') && validate_date($value2, 'Y-m-d H:i:s')) {
                  $sql_search .= " AND " . $field . " BETWEEN '" . $value1 . "' AND '" . $value2 . "'";
                } else if (validate_date($value1) && validate_date($value2)) {
                  $sql_search .= " AND DATE(" . $field . ") BETWEEN '" . $value1 . "' AND '" . $value2 . "'";
                }
              } else {
                if (validate_date($value1, 'Y-m-d H:i:s')) {
                  $sql_search .= " AND " . $field . " " . $comparison . " '" . $value1 . "'";
                } else if (validate_date($value1)) {
                  $sql_search .= " AND DATE(" . $field . ") " . $comparison . " '" . $value1 . "'";
                }
              }
            }
            break;
        }
      }
    }

    return $sql_search;
  }
}

if (!function_exists('filterParams')) {
  function filter_params($params, $field_allowed)
  {
    unset($params['sort']);
    unset($params['page']);
    unset($params['limit']);
    unset($params['search']);
    unset($params['filter']);

    $query_filter = '';

    foreach ($params as $field => $value) {
      if (isset($field_allowed[$field])) {
        $field = $field_allowed[$field];
        if (is_array($value)) {
          foreach ($value as $comparison => $val) {
            if (endsWith($field, 'datetime')) {
              if(validate_date($val)){
                $field = "DATE($field)";
              }else{
                if (!validate_date($val, 'Y-m-d H:i:s')) {
                  $val = '';
                }
              }
              
              if ($comparison == 'le' || $comparison == 'ls' || $comparison == 'lse') {
                $comparison = '';
              }
            }
            if (endsWith($field, 'date')) {
              if (!validate_date($val)) {
                $val = '';
              }
              if ($comparison == 'le' || $comparison == 'ls' || $comparison == 'lse') {
                $comparison = '';
              }
            }
            if ($val != '') {
              switch ($comparison) {
                case 'eq':
                  $query_filter .= " AND $field = '$val' ";
                  break;

                case 'neq':
                  $query_filter .= " AND $field != '$val' ";
                  break;

                case 'lt':
                  $query_filter .= " AND $field < '$val' ";
                  break;

                case 'gt':
                  $query_filter .= " AND $field > '$val' ";
                  break;

                case 'lte':
                  $query_filter .= " AND $field <= '$val' ";
                  break;

                case 'gte':
                  $query_filter .= " AND $field >= '$val' ";
                  break;

                case 'le':
                  $query_filter .= " AND $field LIKE '$val%' ";
                  break;

                case 'ls':
                  $query_filter .= " AND $field LIKE '%$val' ";
                  break;

                case 'lse':
                  $query_filter .= " AND $field LIKE '%$val%' ";
                  break;

                case 'in':
                  $fi = explode(',', $val);
                  for ($q = 0; $q < count($fi); $q++) {
                    $fi[$q] = "'" . $fi[$q] . "'";
                  }
                  $val = implode(',', $fi);
                  $query_filter .= " AND $field IN ($val) ";
                  break;

                case 'nin':
                  $fi = explode(',', $val);
                  for ($q = 0; $q < count($fi); $q++) {
                    $fi[$q] = "'" . $fi[$q] . "'";
                  }
                  $val = implode(',', $fi);
                  $query_filter .= " AND $field NOT IN ($val) ";
                  break;

                default:
                  $query_filter .= " AND $field = '$val' ";
                  break;
              }
            }
          }
        } else {
          if (endsWith($field, 'datetime')) {
            $field = 'date(' . $field . ')';
            if (!validate_date($value)) {
              $value = '';
            }
          }
          if (endsWith($field, 'date')) {
            if (!validate_date($value)) {
              $value = '';
            }
          }
          if ($value != '') {
            $query_filter .= " AND $field = '$value' ";
          }
        }
      }
    }
    return $query_filter;
  }
}

if (!function_exists('genareteFieldQuery')) {
  function generate_field_query($arr)
  {
    $array = array();
    foreach ($arr as $key => $val) {
      if (!is_numeric($key)) {
        $array['sql'][] = $key . ' as \'' . $val . '\'';
        $array['field'][$val] = $key;
      } else {
        $array['sql'][] = $val;
        $array['field'][$val] = $val;
      }
    }
    return $array;
  }
}

if (!function_exists('generateDetailQuery')) {
  function generate_detail_query($query)
  {
    $CI = &get_instance();
    $CI->load->database();

    $query_filter = 'WHERE 1 ';

    $result_arr = array();

    $group_by = isset($query['group_by']) && $query['group_by'] != '' ? 'GROUP BY ' . $query['group_by'] : '';
    $having = isset($query['having']) && $query['having'] != '' ? ' HAVING ' . $query['having'] : '';

    $where_detail = trim($query['where_detail']);
    if (!empty($where_detail)) {
      $query_filter .= " AND $where_detail ";
    }

    $field_show = generate_field_query($query['field_show']);

    $str_field_search = empty($field_show['sql']) ? '*' : implode(',', $field_show['sql']);

    $sql_get = " SELECT
      $str_field_search
      {$query['table_and_join']}
      $query_filter
      $group_by
      $having
    ";

    $result = $CI->db->query($sql_get);

    $result_arr = array();

    if ($result->num_rows() > 0) {
      $data_detail = $result->row_array();
      $result_arr = sanitization_response($data_detail);
    }

    return $result_arr;
  }
}

if (!function_exists('sanitizationResponse')) {
  function sanitization_response($data)
  {
    foreach ($data as $key => $val) {
      if (is_null($val)) {
        $data[$key] = '';
      }
      if (endsWith($key, 'jsonobject')) {
        $data[$key] = json_decode(empty($val) ? '{}' : $val);
      }
      if (endsWith($key, 'jsonarray')) {
        $data[$key] = json_decode(empty($val) ? '[]' : $val);
      }
      if (endsWith($key, 'date')) {
        // $value = '';
        // if (!empty($val) && $val != '0000-00-00') {
        //     $value = strftime('%d %B %Y', strtotime($val));
        // }
        // $data[$key . '_format'] = $value;
        if ($val == '0000-00-00') {
          $data[$key] = '';
        }
      }
      if (endsWith($key, 'datetime')) {
        // $value = '';
        // if (!empty($val) && $val != '0000-00-00 00:00:00') {
        //     $value = strftime('%A, %d %B %Y %H:%M', strtotime($val));
        // }
        // $data[$key . '_format'] = $value;
        if ($val == '0000-00-00 00:00:00') {
          $data[$key] = '';
        }
      }
      if (endsWith($key, 'bool')) {
        $data[$key] = $val == '1';
      }
      // if (is_numeric($val) && !endsWith($key, 'id') && !strstr($key, 'is') && !endsWith($key, 'phone')) {
      //   $data[$key . '_format'] = number_format($val, 0, ',', '.');
      //   $data[$key . '_sformat'] = nominal_convert($val);
      // }
    }
    return $data;
  }
}

if (!function_exists('generateDataQuery')) {
  function generate_data_query($params, $query)
  {
    $CI = &get_instance();
    $CI->load->database();
    
    $from_table_and_join = $query['table_and_join'];
    $field_show = $query['field_show'];
    $where_detail = isset($query['where_detail']) ? $query['where_detail'] : '';
    $group_by = isset($query['group_by']) && $query['group_by'] != '' ? 'GROUP BY ' .$query['group_by'] : '';
    $having = isset($query['having']) && $query['having'] != '' ? ' HAVING ' . $query['having'] : '';
    $field_search = isset($query['field_search']) ? $query['field_search'] : array();
    
    $field_show = generate_field_query($field_show);

    $limit = 10;
    if(isset($query['limit'])){
      $limit = $query['limit'];
    }
    if (isset($params['limit'])) {
      $limit = (int) $params['limit'] <= 0 ? $limit : (int)$params['limit'];
    }

    $page = 1;
    if (isset($params['page'])) {
      $page = (int) $params['page'] <= 0 ? 1 : (int)$params['page'];
    }

    $sort = isset($params['sort']) ? $params['sort'] : '';
    $search = isset($params['search']) ? $params['search'] : '';

    $start = ($page - 1) * $limit;

    $query_filter = 'WHERE 1 ';

    $result_arr = array();

    $query_filter .= filter_params($params, $field_show['field']);
    if (isset($params['filter'])) {
      $query_filter .= filter_params_array($params['filter'], $field_show['field']);
    }
    $query_filter .= search_query($search, $field_search);

    $where_detail = trim($where_detail);
    if (!empty($where_detail)) {
      $query_filter .= " AND $where_detail ";
    }

    $dir = 'ASC';
    if (isset($query['sort'])) {
      $dir = $query['sort'];
    }

    if (startsWith($sort, '-')) {
      $dir = 'DESC';
      $sort = str_replace('-', '', $sort);
    }

    if (!isset($field_show['field'][$sort])) {
      if (isset($query['order']) && $query['order'] != '') {
        $sort = $query['order'];
      }else{
        $sort = reset($field_show['field']) ? reset($field_show['field']) : '';
      }
    }

    $order_by = '';
    if ($sort != '') {
      $order_by = "ORDER BY $sort $dir";
    }

    $str_field_search = implode(',', $field_show['sql']);

    $pagination = true;
    if(isset($query['pagination'])){
      $pagination = $query['pagination'];
    }

    $query_limit = "LIMIT $start, $limit";
    if(!$pagination){
      $query_limit = '';
    }

    $sql_get = " SELECT
      $str_field_search
      $from_table_and_join
      $query_filter
      $group_by
      $having
      $order_by
      $query_limit
    ";

    $sql_count = " SELECT 1 as total 
      $from_table_and_join
      $query_filter
      $group_by
      $having
    ";

    $result = $CI->db->query($sql_get);

    $total = $CI->db->query($sql_count)->num_rows();

    $result_arr['data'] = array();

    if ($result->num_rows() > 0) {
      foreach ($result->result_array() as $row) {
        $result_arr['data'][] = sanitization_response($row);
      }
    }

    if($pagination){
      $result_arr['pagination'] = page_generate($total, $page, $limit);
    }

    return $result_arr;
  }
}
