<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Core_Controller {


	function index()
	{
		$data = array();
		$this->template->title('Home');
		$this->template->content("home_view", $data);
		$this->template->show("template_view");
	}

	function cart()
	{
		$data = array();
		$this->template->title('Cart');
		$this->template->content("cart_view", $data);
		$this->template->show("template_view");
	}

	public function insert(){
		// $this->form_validation->set_rules('image_url', 'Gambar', 'required|valid_url');
		$this->form_validation->set_rules('name', 'Nama Produk', 'required');
		$this->form_validation->set_rules('stok', 'Stok Produk', 'required|is_natural');
		$this->form_validation->set_rules('price', 'Harga Produk', 'required|is_natural_no_zero');

		if ($this->form_validation->run($this) == false) {
			$this->responseValidationError($this->form_validation);
		} else {
			$is_error = false;
			$this->db->trans_begin();
			$error_code = '';
			$datetime = date('Y-m-d H:i:s');
			$data = array();

			try {

				$data = array();
				$data['product_code'] = $this->generateProductCode($this->input->post('name'));
				$data['product_name'] = $this->input->post('name');
				$data['product_stok'] = $this->input->post('stok');
				$data['product_price'] = $this->input->post('price');
				$data['product_create_datetime'] = $datetime;
				$data['product_update_datetime'] = $datetime;

				if (!$this->db->insert('product', $data)) {
					throw new Exception("Gagal insert data", 1);
				}
				$data['product_id'] = $this->db->insert_id();
			} catch (Exception $ex) {
				$is_error = true;
				$error_code = $ex->getMessage();
			}

			if (!$is_error) {
				if ($this->db->trans_status() === false) {
					$this->db->trans_rollback();
					$this->responseWithoutData(400, 'Gagal tambah data! Silahkan coba lagi.', $error_code);
				} else {
					$this->db->trans_commit();
					$this->responseSingleData(200, 'Berhasil tambah data.', sanitization_response($data));
				}
			} else {
				$this->db->trans_rollback();
				$this->responseWithoutData(400, 'Gagal tambah data! Silahkan coba lagi.', $error_code);
			}
		}
	}

	public function get_data(){
		// **WAJIB** table form & join
		$query['table_and_join'] = "FROM product";
		// query where yg selalu ada
		$query['where_detail'] = "";
		// query group by
		$query['group_by'] = "";
		// query having jika menggunakan group by
		$query['having'] = "";

		// **WAJIB** field yang di select key= field DB, value= key response
		$query['field_show'] = array(
			'product_id' => 'id',
			'product_name' => 'name',
			'product_code' => 'code',
			'product_price' => 'price',
			'product_stok' => 'stok',
			'product_image_url' => 'image_url',
		);

		// *WAJIB ARRAY* field yang di search menggunakan field search
		$query['field_search'] = array(
			'product_name',
			'product_code',
		);

		// default order by (default sistem index 0 dari field_show)
		$query['order'] = 'product_name';
		// default sort direction (default sistem ASC)
		$query['sort'] = 'ASC';
		// default limit (default sistem 10)
		$query['limit'] = 10;
		// menggunakan pagination / tidak (default sistem true)
		$query['pagination'] = true;

		$data = generate_data_query($this->input->get(), $query);

		$this->responseMultiData(200, 'Data Produk', $data);
	}

	public function get_cart(){
		// **WAJIB** table form & join
		$query['table_and_join'] = "FROM cart JOIN product ON product_id = cart_product_id";
		// query where yg selalu ada
		$query['where_detail'] = "";
		// query group by
		$query['group_by'] = "";
		// query having jika menggunakan group by
		$query['having'] = "";

		// **WAJIB** field yang di select key= field DB, value= key response
		$query['field_show'] = array(
			'cart_id' => 'id',
			'product_id' => 'product_id',
			'product_name' => 'name',
			'cart_price' => 'price',
			'cart_qty' => 'qty',
			'cart_subtotal' => 'subtotal',
		);

		// *WAJIB ARRAY* field yang di search menggunakan field search
		$query['field_search'] = array(
			'product_name',
		);

		// default order by (default sistem index 0 dari field_show)
		$query['order'] = 'cart_id';
		// default sort direction (default sistem ASC)
		$query['sort'] = 'ASC';
		// default limit (default sistem 10)
		$query['limit'] = 10;
		// menggunakan pagination / tidak (default sistem true)
		$query['pagination'] = false;

		$data = generate_data_query($this->input->get(), $query);

		$this->responseMultiData(200, 'Data Produk', $data);
	}

	private function generateProductCode($name){
		$arr = explode(' ', $name);
		if(count($arr) > 1){
			$prefix = substr($arr[0], 0, 1). substr($arr[1], 0, 1);
		}else{
			$prefix = substr($arr[0], 0, 2);
		}

		$sql = "
      SELECT
      IFNULL(LPAD(MAX(CAST(RIGHT(product_code, 3) AS SIGNED) + 1), 3, '0'), '" . sprintf('%03d', 1) . "') AS code 
      FROM product
    ";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$row = $query->row();
			return strtoupper($prefix) . $row->code;
		} else {
			return strtoupper($prefix) . '';
		}
	}

	public function buy(){
		$this->form_validation->set_rules('id', 'ID Produk', 'required');

		if ($this->form_validation->run($this) == false) {
			$this->responseValidationError($this->form_validation);
		} else {
			$is_error = false;
			$this->db->trans_begin();
			$error_code = '';
			$datetime = date('Y-m-d H:i:s');
			$data = array();

			try {

				$sql_product = "SELECT * FROM product WHERE product_id = '{$this->input->post('id')}'";
				$product = $this->db->query($sql_product)->row();

				if(empty($product)){
					throw new Exception("Gagal Update Data", 1);
				}

				if($product->product_stok <= 0){
					throw new Exception("Gagal Update Data", 1);
				}

				$sql_check = "SELECT * FROM cart WHERE cart_product_id = '{$this->input->post('id')}'";
				$query_check = $this->db->query($sql_check);

				if($query_check->num_rows() > 0){
					$data = array();
					$data['cart_qty'] = $query_check->row('cart_qty') + 1;
					$data['cart_subtotal'] = $data['cart_qty'] * $product->product_price;

					$this->db->where('cart_id', $query_check->row('cart_id'));
					$this->db->update('cart', $data);

					if ($this->db->affected_rows() < 0) {
						throw new Exception("Gagal Update Data", 1);
					}
				}else{

					$data = array();
					$data['cart_product_id'] = $this->input->post('id');
					$data['cart_price'] = $product->product_price;
					$data['cart_subtotal'] = $product->product_price;
					$data['cart_qty'] = 1;
	
					if (!$this->db->insert('cart', $data)) {
						throw new Exception("Gagal insert data", 1);
					}
				}

				$this->db->set('product_stok', 'product_stok-1', FALSE);
				$this->db->where('product_id', $this->input->post('id'));
				$this->db->update('product');

				if ($this->db->affected_rows() < 0) {
					throw new Exception("Gagal Update Data", 1);
				}

			} catch (Exception $ex) {
				$is_error = true;
				$error_code = $ex->getMessage();
			}

			if (!$is_error) {
				if ($this->db->trans_status() === false) {
					$this->db->trans_rollback();
					$this->responseWithoutData(400, 'Gagal tambah data! Silahkan coba lagi.', $error_code);
				} else {
					$this->db->trans_commit();
					$this->responseSingleData(200, 'Berhasil tambah data.');
				}
			} else {
				$this->db->trans_rollback();
				$this->responseWithoutData(400, 'Gagal tambah data! Silahkan coba lagi.', $error_code);
			}
		}
	}

	public function add_cart()
	{
		$this->form_validation->set_rules('id', 'ID Produk', 'required');

		if ($this->form_validation->run($this) == false) {
			$this->responseValidationError($this->form_validation);
		} else {
			$is_error = false;
			$this->db->trans_begin();
			$error_code = '';
			$datetime = date('Y-m-d H:i:s');
			$data = array();

			try {

				$sql_product = "SELECT * FROM product WHERE product_id = '{$this->input->post('id')}'";
				$product = $this->db->query($sql_product)->row();

				if (empty($product)) {
					throw new Exception("Gagal Update Data", 1);
				}

				if ($product->product_stok < 1) {
					throw new Exception("Gagal Update Data", 1);
				}

				$sql_check = "SELECT * FROM cart WHERE cart_product_id = '{$this->input->post('id')}'";
				$query_check = $this->db->query($sql_check);

				if ($query_check->num_rows() > 0) {
					$data = array();
					$data['cart_qty'] = $query_check->row('cart_qty') + 1;
					$data['cart_subtotal'] = $data['cart_qty'] * $product->product_price;

					$this->db->where('cart_id', $query_check->row('cart_id'));
					$this->db->update('cart', $data);

					if ($this->db->affected_rows() < 0) {
						throw new Exception("Gagal Update Data", 1);
					}

					$this->db->set('product_stok', 'product_stok-1', FALSE);
					$this->db->where('product_id', $this->input->post('id'));
					$this->db->update('product');

					if ($this->db->affected_rows() < 0) {
						throw new Exception("Gagal Update Data", 1);
					}

				} else {
						throw new Exception("Gagal insert data", 1);
				}
			} catch (Exception $ex) {
				$is_error = true;
				$error_code = $ex->getMessage();
			}

			if (!$is_error) {
				if ($this->db->trans_status() === false) {
					$this->db->trans_rollback();
					$this->responseWithoutData(400, 'Gagal tambah data! Silahkan coba lagi.', $error_code);
				} else {
					$this->db->trans_commit();
					$this->responseSingleData(200, 'Berhasil tambah data.');
				}
			} else {
				$this->db->trans_rollback();
				$this->responseWithoutData(400, 'Gagal tambah data! Silahkan coba lagi.', $error_code);
			}
		}
	}
	public function minus_cart()
	{
		$this->form_validation->set_rules('id', 'ID Produk', 'required');

		if ($this->form_validation->run($this) == false) {
			$this->responseValidationError($this->form_validation);
		} else {
			$is_error = false;
			$this->db->trans_begin();
			$error_code = '';
			$datetime = date('Y-m-d H:i:s');
			$data = array();

			try {

				$sql_product = "SELECT * FROM product WHERE product_id = '{$this->input->post('id')}'";
				$product = $this->db->query($sql_product)->row();

				if (empty($product)) {
					throw new Exception("Gagal Update Data 1", 1);
				}

				$sql_check = "SELECT * FROM cart WHERE cart_product_id = '{$this->input->post('id')}'";
				$query_check = $this->db->query($sql_check);

				
				if ($query_check->num_rows() > 0) {
					if($query_check->row('cart_qty') <= 1){
						throw new Exception("Gagal Update Data 1", 1);
					}
					$data = array();
					$data['cart_qty'] = $query_check->row('cart_qty') - 1;
					$data['cart_subtotal'] = $data['cart_qty'] * $product->product_price;

					$this->db->where('cart_id', $query_check->row('cart_id'));
					$this->db->update('cart', $data);

					if ($this->db->affected_rows() < 0) {
						throw new Exception("Gagal Update Data 3", 1);
					}

					$this->db->set('product_stok', 'product_stok+1', FALSE);
					$this->db->where('product_id', $this->input->post('id'));
					$this->db->update('product');

					if ($this->db->affected_rows() < 0) {
						throw new Exception("Gagal Update Data 5", 1);
					}
				} else {
					throw new Exception("Gagal insert data 4", 1);
				}
			} catch (Exception $ex) {
				$is_error = true;
				$error_code = $ex->getMessage();
			}

			if (!$is_error) {
				if ($this->db->trans_status() === false) {
					$this->db->trans_rollback();
					$this->responseWithoutData(400, 'Gagal tambah data! Silahkan coba lagi.', $error_code);
				} else {
					$this->db->trans_commit();
					$this->responseSingleData(200, 'Berhasil tambah data.');
				}
			} else {
				$this->db->trans_rollback();
				$this->responseWithoutData(400, 'Gagal tambah data! Silahkan coba lagi.', $error_code);
			}
		}
	}

	public function remove_cart(){
		$this->form_validation->set_rules('id', 'ID Produk', 'required');

		if ($this->form_validation->run($this) == false) {
			$this->responseValidationError($this->form_validation);
		} else {
			$is_error = false;
			$this->db->trans_begin();
			$error_code = '';
			$datetime = date('Y-m-d H:i:s');
			$data = array();

			try {

				$sql_check = "SELECT * FROM cart WHERE cart_id = '{$this->input->post('id')}'";
				$query_check = $this->db->query($sql_check);

				if ($query_check->num_rows() > 0) {
					
					$this->db->where('cart_id', $query_check->row('cart_id'));
					$this->db->delete('cart');

					if ($this->db->affected_rows() < 0) {
						throw new Exception("Gagal Update Data 3", 1);
					}

					$this->db->set('product_stok', 'product_stok+'.$query_check->row('cart_qty'), FALSE);
					$this->db->where('product_id', $this->input->post('id'));
					$this->db->update('product');

					if ($this->db->affected_rows() < 0) {
						throw new Exception("Gagal Update Data 5", 1);
					}
					
				} else {
					throw new Exception("Gagal insert data 4", 1);
				}
			} catch (Exception $ex) {
				$is_error = true;
				$error_code = $ex->getMessage();
			}

			if (!$is_error) {
				if ($this->db->trans_status() === false) {
					$this->db->trans_rollback();
					$this->responseWithoutData(400, 'Gagal tambah data! Silahkan coba lagi.', $error_code);
				} else {
					$this->db->trans_commit();
					$this->responseSingleData(200, 'Berhasil tambah data.');
				}
			} else {
				$this->db->trans_rollback();
				$this->responseWithoutData(400, 'Gagal tambah data! Silahkan coba lagi.', $error_code);
			}
		}
	}
}
